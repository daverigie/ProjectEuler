def sum_multiples(n, N):

	"""
	find multiples of n below N
	"""

	x = n
	total = 0
	while x<N:
		total += x
		x += n

	return total

answer = (sum_multiples(3, 1000) + 
		  sum_multiples(5,1000)   -
		  sum_multiples(15, 1000))

print(answer)