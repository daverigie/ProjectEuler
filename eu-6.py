def difference(N):

	s1 = 0
	s2 = 0
	for i in xrange(1,N+1):
		s1 += i**2
		s2 += i

	s2 *= s2
	return s2-s1
