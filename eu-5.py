import math

def isPrime(x):

	if x == 1:
		return False
	elif x == 2:
		return True
	elif x == 3:
		return True

	if x%2 == 0:
		return False

	y = 3

	while y<=math.sqrt(x):

		if x%y == 0:
			return False

		y += 2

	return True

def primes(N):

	if N < 2:
		return 

	yield 2

	x = 3
	while x<=N:
		if isPrime(x):
			yield x
		x += 2

	return


def primeFactor(x):

	factors = []
	for p in primes(x):
		
		while x%p == 0:
			x /= p
			factors.append(p)

	return factors

def smallestMultiple(N=20):

	pp = primes(N)
	pcount = {}

	for p in pp:
		pcount[p] = 0

	for i in xrange(1, N+1):
		primefacs = primeFactor(i)
		for p in primefacs:
			num = primefacs.count(p)
			if pcount[p] < num:
				pcount[p] = num

	val = 1
	for key in pcount.keys():
		if pcount[key] > 0:
			val *= key**pcount[key]
		

	return val





