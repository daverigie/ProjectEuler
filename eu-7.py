import math

def isPrime(x):

	if x == 1:
		return False
	elif x == 2:
		return True
	elif x == 3:
		return True

	if x%2 == 0:
		return False

	y = 3

	while y<=math.sqrt(x):

		if x%y == 0:
			return False

		y += 2

	return True

def primes(N):

	if N < 2:
		return 

	yield 2

	x = 3
	while x<=N:
		if isPrime(x):
			yield x
		x += 2

	return

def NthPrime(N):

	counter = 0
	if N < 1:
		return

	for p in primes(1e100):
		counter += 1
		if counter == N:
			return p
