import math

def isPrime(x):

	if x == 1:
		return False
	elif x == 2:
		return True
	elif x == 3:
		return True

	y = 2

	while y<=math.sqrt(x):

		if x%y == 0:
			return False

		y += 1

	return True


def largest_prime_factor(N):

	# Start with odd number <= sqrt(N)
	x = int(math.sqrt(N))
	if x%2 == 0:
		x -= 1

	while x >= 3:
		if N%x == 0:
			if isPrime(x):
				return x	
		x -= 2

	if N%2 == 0:
		return 2

	return -1

answer = largest_prime_factor(600851475143)
print(answer)