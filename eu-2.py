def fib_sum(N):

	first = 1
	second = 2

	total = 0

	while second < N:

		if second%2 == 0:
			total += second

		temp = second
		second += first
		first = temp

	return total


answer = fib_sum(int(4e6))

print(answer)